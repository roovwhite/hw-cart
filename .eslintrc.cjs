module.exports = {
  root: true,
  env: { browser: true, es2020: true },
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended-type-checked",
    "plugin:react-hooks/recommended",
    "plugin:react/jsx-runtime",
    "plugin:react/recommended",
    "prettier",
  ],
  ignorePatterns: ["dist", ".eslintrc.cjs", "tailwind.config.js",],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
    project: ["./tsconfig.json", "./tsconfig.node.json"],
    tsconfigRootDir: __dirname,
  },
  plugins: [
    "react-refresh",
    "@typescript-eslint",
    "prettier",
  ],
  rules: {
    "react-refresh/only-export-components": ["warn", {
      allowConstantExport: true
    }],
    "react/jsx-uses-react": "off",
    "react/react-in-jsx-scope": "off",
    "@typescript-eslint/no-non-null-assertion": "error",
    "no-console": "warn",
    "semi-style": ["error", "last"],
    "max-len": ["error", {
      "code": 120,
      "tabWidth": 2
    }],
    "object-curly-spacing": ["error", "always"],
    "semi": ["error", "always"],
    "no-var": 2,
    "prettier/prettier": "error",
  },
  settings: {
    "react": {
      "version": "detect",
    }
  },
}
